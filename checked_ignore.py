import os
import argparse
import re

def get_ignored_files(project_dir, gitignore_path):
    ignored_files = []
    with open(gitignore_path, 'r') as gitignore:
        patterns = gitignore.read().splitlines()
        for root, dirs, files in os.walk(project_dir):
            for file in files:
                filepath = os.path.join(root, file)
                for pattern in patterns:
                    if pattern.startswith('*'):
                        if re.match(pattern[1:], file):
                            ignored_files.append((filepath, f"ignored by expression {pattern}"))
                            break
                    elif pattern.startswith('/'):
                        pattern = pattern[1:]
                        if filepath.endswith(pattern):
                            ignored_files.append((filepath, f"ignored by expression {pattern}"))
                            break
                    else:
                        if filepath.endswith(pattern):
                            ignored_files.append((filepath, f"ignored by expression {pattern}"))
                            break
    return ignored_files

def main():
    parser.add_argument('--project_dir', type=str, required=True)
    args = parser.parse_args()

    gitignore_path = os.path.join(args.project_dir, '.gitignore')

    ignored_files = get_ignored_files(args.project_dir, gitignore_path)

    print("Ignored files:")
    for ignored_file, expression in ignored_files:
        print(f"{ignored_file} {expression}")

if __name__ == "__main__":
    main()
