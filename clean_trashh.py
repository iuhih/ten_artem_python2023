import os
import argparse
import time


def clean_trash(trash_folder_path, age_thr, log_file):
    deleted_files = []

    for root, dirs, files in os.walk(trash_folder_path, topdown=False):
        for file in files:
            file_path = os.path.join(root, file)
            file_age = time.time() - os.path.getmtime(file_path)
            if file_age > age_thr:
                os.remove(file_path)
                deleted_files.append(file_path)
        for dir in dirs:
            dir_path = os.path.join(root, dir)
            try:
                os.rmdir(dir_path)
                deleted_files.append(dir_path)
            except OSError:
                pass

    with open(log_file, 'a') as f:
        for item in deleted_files:
            f.write("%s\n" % item)


def main():
    parser = argparse.ArgumentParser(description='Clean trash folder')
    parser.add_argument('--trash_folder_path', type=str, help='Path to the trash folder', required=True)
    parser.add_argument('--age_thr', type=int, help='Threshold age for deleting files in seconds', required=True)
    args = parser.parse_args()

    log_file = 'clean_trash.log'

    while True:
        clean_trash(args.trash_folder_path, args.age_thr, log_file)
        print("Trash cleaned. Sleeping for 50 second...")
        time.sleep(50)

if __name__ == "__main__":
    main()
