from flask import Flask, jsonify, request, render_template_string
import pandas as pd
from typing import Dict, List, Union

app = Flask(__name__)

hw_01 = pd.read_csv('hw1.csv')
hw_02 = pd.read_csv('hw2.csv')

homeworks = {
    'hw-01': hw_01,
    'hw-02': hw_02,
}

students = pd.concat([
    hw_01[['student_id', 'ФИ', 'Группа']],
    hw_02[['student_id', 'ФИ', 'Группа']]
]).drop_duplicates().rename(columns={'ФИ': 'name', 'Группа': 'group_id'})

def calculate_mark(score: float) -> str:
    if score < 1:
        return 'Be_better'
    elif 1 <= score < 30:
        return '3'
    elif 30 <= score < 50:
        return '4'
    elif 50 <= score < 70:
        return '5'
    return 'Invalid score'

@app.route('/names', methods=['GET'])
def get_names() -> Dict[str, List[str]]:
    names = students['name'].tolist()
    return jsonify({'names': names})

@app.route('/<string:hw_name>/mean_score', methods=['GET'])
def get_mean_score(hw_name: str) -> Union[Dict[str, float], tuple]:
    mean_score = homeworks[hw_name]['Баллы'].mean()
    return jsonify({'mean_score': mean_score})

@app.route('/mean_score', methods=['GET'])
def get_mean_score_with_params() -> Union[Dict[str, float], tuple]:
    group_id = request.args.get('group_id', type=int)
    hw_name = request.args.get('hw_name')
    group_scores = homeworks[hw_name][homeworks[hw_name]['Группа'] == group_id]['Баллы']
    mean_score = group_scores.mean()
    return jsonify({'mean_score': mean_score})

@app.route('/mark', methods=['GET'])
def get_mark() -> Union[Dict[str, str], tuple]:
    student_id = request.args.get('student_id', type=int)
    group_id = request.args.get('group_id', type=int)
    if student_id:
        total_score = sum(hw[hw['student_id'] == student_id]['Баллы'].sum() for hw in homeworks.values())
        mark = calculate_mark(total_score)
        return jsonify({'mark': mark})
    elif group_id:
        group_scores = students[students['group_id'] == group_id].apply(
            lambda x: sum(hw[hw['student_id'] == x['student_id']]['Баллы'].sum() for hw in homeworks.values()), axis=1
        )
        mean_score = group_scores.mean()
        mark = calculate_mark(mean_score)
        return jsonify({'mean_mark': mark})
    else:
        return {'status': 'error', 'message': 'Missing parameters'}, 400

@app.route('/course_table', methods=['GET'])
def get_course_table() -> Union[str, tuple]:
    hw_name = request.args.get('hw_name')
    group_id = request.args.get('group_id', type=int)
    table_data = homeworks[hw_name] if not group_id else homeworks[hw_name][homeworks[hw_name]['Группа'] == group_id]
    html_table = table_data.to_html()
    return render_template_string(html_table)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1337)
#dfgf